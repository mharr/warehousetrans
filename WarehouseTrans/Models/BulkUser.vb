﻿
Imports WarehouseTrans.Extensions

Namespace Models
    Public Class BulkUser
        Public UID As Integer
        Public Name As String
        Public FirstName As String
        Public LastName As String
        Public Plant As String
        Public Enabled as Boolean
        Public CanTransfer As Boolean 
        Public CanBinMove As Boolean 
        Public CanWeigh As Boolean 
        Public CanHotlist As Boolean
        Public CanMoveBulk As Boolean 
        Public CanMaterialAdjust As Boolean 
        Public CanEditUsers As Boolean 
        Public CanPrintLists As Boolean 
        Public CanCreatePallet As Boolean 
        Public CanPalletMove As Boolean 
        Public CanPlantTransfer As Boolean 
        Public IsAdmin As Boolean 
        Public ActiveDate As DateTime
        Public ADName as String

        Public Sub New()

        End Sub

        Public Sub New(rdr as IDataRecord)
            UID = IDataRecordExtensions.SafeGetInt32(rdr, "UID")
            Name = IDataRecordExtensions.SafeGetString(rdr, "Name")
            FirstName = IDataRecordExtensions.SafeGetString(rdr, "FirstName")
            LastName = IDataRecordExtensions.SafeGetString(rdr, "LastName")
            Plant = IDataRecordExtensions.SafeGetString(rdr, "Plant")
            Enabled = IDataRecordExtensions.SafeGetBoolean(rdr, "Enabled")
            CanTransfer = IDataRecordExtensions.SafeGetBoolean(rdr, "CanTransfer")
            CanBinMove = IDataRecordExtensions.SafeGetBoolean(rdr, "CanBinMove")
            CanWeigh = IDataRecordExtensions.SafeGetBoolean(rdr, "CanWeigh")
            CanHotlist = IDataRecordExtensions.SafeGetBoolean(rdr, "CanHotlist")
            CanMoveBulk = IDataRecordExtensions.SafeGetBoolean(rdr, "CanMoveBulk")
            CanMaterialAdjust = IDataRecordExtensions.SafeGetBoolean(rdr, "CanMaterialAdjust")
            CanEditUsers = IDataRecordExtensions.SafeGetBoolean(rdr, "CanEditUsers")
            CanPrintLists = IDataRecordExtensions.SafeGetBoolean(rdr, "CanPrintLists")
            CanCreatePallet = IDataRecordExtensions.SafeGetBoolean(rdr, "CanCreatePallet")
            CanPalletMove = IDataRecordExtensions.SafeGetBoolean(rdr, "CanPalletMove")
            CanPlantTransfer = IDataRecordExtensions.SafeGetBoolean(rdr, "CanPlantTransfer")
            IsAdmin = IDataRecordExtensions.SafeGetBoolean(rdr, "IsAdmin")
            ActiveDate = IDataRecordExtensions.SafeGetDateTime(rdr, "ActiveDate")
            ADName = IDataRecordExtensions.SafeGetString(rdr, "ADName")
        End Sub
    End Class
End NameSpace