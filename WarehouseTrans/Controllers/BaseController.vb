﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Security.Principal
Imports System.Web
Imports System.Web.Mvc
Imports WarehouseTrans.Extensions

Namespace Controllers
    Public Class BaseController
        Inherits Controller

        Protected Overridable ReadOnly Shadows Property User As MSSPrincipal
            Get
                Return TryCast(HttpContext.User, MSSPrincipal)
            End Get
        End Property
    End Class
End Namespace