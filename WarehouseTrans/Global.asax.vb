﻿Imports System.Security.Principal
Imports System.Threading
Imports System.Web.Http
Imports System.Web.Optimization
Imports WarehouseTrans.Extensions

Public Class MvcApplication
    Inherits System.Web.HttpApplication

    Protected Sub Application_Start()
        AreaRegistration.RegisterAllAreas()
        GlobalConfiguration.Configure(AddressOf WebApiConfig.Register)
        FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters)
        RouteConfig.RegisterRoutes(RouteTable.Routes)
        BundleConfig.RegisterBundles(BundleTable.Bundles)
    End Sub

    Protected Sub Application_PostAuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        Dim newUser = New MSSPrincipal(CType(HttpContext.Current.User, WindowsPrincipal))
        Thread.CurrentPrincipal = newUser
        If HttpContext.Current IsNot Nothing Then
            HttpContext.Current.User = newUser
        End If 
    End Sub

End Class
