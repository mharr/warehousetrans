﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Threading
Imports ADODB

Namespace Data

    Module mobDatabase
        Public objConn As SqlConnection
        Public objConnUZ As SqlConnection
        Public objAccess As ADODB.Connection
        Public objConnFSPRD As SqlConnection

        Dim pass = "E4V2G7xgBUPXRcAztCO2"
        Public uzConnString As String = AES_Decrypt(My.Settings.mySIPConnString, pass)
        Public fsprdConnString As String = AES_Decrypt(My.Settings.fsprdConnString, pass)
        Public mssmmConnString As String = AES_Decrypt(My.Settings.mssmmConnString, pass)

        Dim nMessageCount As Integer = 0
        Dim sSQLQueue As String = ""

        Public Function ConnUZDB() As Boolean
            Try
                ConnectDatabase(uzConnString, objConnUZ)
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function ConnAllDB() As Boolean
            Try
                ConnectDatabase(fsprdConnString, objConnFSPRD)
                ConnectDatabase(mssmmConnString, objConn)
                ConnectDatabase(uzConnString, objConnUZ)
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function ConnDB(ByVal serverIndex As Integer, Optional ByVal secondRun As Boolean = False) As Boolean
            Try
                ConnectDatabase(fsprdConnString, objConnFSPRD)
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function ConnectDatabase(connectionString As String, ByRef connectionObj As SqlConnection)
            If Not connectionObj Is Nothing Then
                If connectionObj.State = ConnectionState.Open Then
                    Return True
                End If
            End If
            Try
                connectionObj = New SqlConnection(connectionString)
                connectionObj.Open()
                connectionObj.Close()
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function SqlQuery(ByVal sqlStatement As String, ByVal databaseConnectionObject As SqlConnection, Optional ByVal timeout As Integer = 300) As DataTable
            Dim dc As SqlCommand
            Dim dr As SqlDataReader
            Try

                If databaseConnectionObject.State <> ConnectionState.Open Then databaseConnectionObject.Open()

                SqlQuery = New DataTable
                'Dim da As New SqlDataAdapter(sqlStatement, databaseConnectionObject)
                dc = New SqlCommand(sqlStatement, databaseConnectionObject) With {
                    .CommandTimeout = timeout
                    }
                dr = Nothing
                dr = dc.ExecuteReader
                SqlQuery.Load(dr)
                dc = Nothing
            Catch ex As Exception
                SqlQuery = Nothing
                Debug.Print(ex.Message)
                dr = Nothing
            Finally
                databaseConnectionObject.Close()
            End Try
        End Function
        Public Function CStrX(ByVal inputValue As Object) As String
            Try
                inputValue = Convert.ToString(inputValue)
                If inputValue = Nothing Then
                    CStrX = ""
                Else
                    CStrX = CStr(inputValue)
                End If
            Catch
                CStrX = ""
            End Try
        End Function
        Public Function CDBValid(ByVal inputValue As Object) As String
            CDBValid = Trim(CStrX(inputValue))
            CDBValid = Replace(CDBValid, "''", "'")
            '    CDBValid = Replace(CDBValid, """", """""")
            CDBValid = Replace(CDBValid, "'", "''")
            CDBValid = Replace(CDBValid, Chr(0), "")
            CDBValid = Replace(CDBValid, vbCrLf, "^")
            CDBValid = Replace(CDBValid, "#N/A", "")

            If CDBValid Is Nothing Then
                CDBValid = ""
            End If
        End Function
        Public Sub SqlExecute(ByVal sqlStatement As String, ByVal databaseConnectionObject As SqlConnection, Optional ByVal messageLen As Integer = 5, Optional ByVal force As Boolean = True)
            nMessageCount = nMessageCount + 1
            If sqlStatement <> "" Then
                sSQLQueue = sSQLQueue & sqlStatement & ";"
            End If

            If databaseConnectionObject.State <> ConnectionState.Open Then databaseConnectionObject.Open()

            If (nMessageCount >= messageLen Or force = True) And sSQLQueue <> "" And (databaseConnectionObject IsNot Nothing AndAlso databaseConnectionObject.State = ConnectionState.Open) Then
                Dim Command As SqlCommand = databaseConnectionObject.CreateCommand()
                Command.CommandTimeout = 6000
                Command.CommandText = sSQLQueue ' Replace(Replace(sqlStatement, "'", "''"), """", "''")
                Try
                    Command.ExecuteNonQuery()

                Catch ex As Exception
                    Debug.Print(ex.Message)
                    Debug.Print(sqlStatement)
                End Try

                sSQLQueue = ""
                nMessageCount = 0
            End If

            databaseConnectionObject.Close()

        End Sub
        Public Function CDateX(ByVal inputValue As Object) As String
            Try
                inputValue = Convert.ToString(inputValue)
                If inputValue = Nothing Then
                    CDateX = "NULL"
                Else
                    If CStr(inputValue) = "00/00/0000" Or CStr(inputValue) = "00000000" Then
                        CDateX = "NULL"
                    ElseIf Len(inputValue) = 8 Then
                        CDateX = Mid(inputValue, 5, 2) & "/" & Mid(inputValue, 7, 2) & "/" & Mid(inputValue, 1, 4)
                    ElseIf Len(inputValue) = 6 Then
                        CDateX = "01/01/1900 " & Mid(inputValue, 1, 2) & ":" & Mid(inputValue, 3, 2) & ":" & Mid(inputValue, 5, 2)
                    ElseIf IsDate(inputValue) = True Then
                        CDateX = CStr(inputValue)
                    Else
                        CDateX = "NULL"
                    End If
                End If

            Catch
                CDateX = "NULL"
            End Try
        End Function
        Public Function CLngX(ByVal inputValue As Object) As Long
            Try
                If IsDBNull(inputValue) = True Then
                    CLngX = 0
                ElseIf String.IsNullOrEmpty(inputValue) = True Then
                    CLngX = 0
                ElseIf Trim(inputValue) = "" Then
                    CLngX = 0
                ElseIf inputValue Is Nothing Then
                    CLngX = 0
                ElseIf IsNumeric(inputValue) = True Then
                    CLngX = CLng(inputValue)
                Else
                    CLngX = 0
                End If
            Catch
                CLngX = 0
            End Try
        End Function
        Public Function CSngX(ByVal inputValue As Object) As Single
            Try
                If IsDBNull(inputValue) = True Then
                    CSngX = 0
                ElseIf String.IsNullOrEmpty(inputValue) = True Then
                    CSngX = 0
                ElseIf Trim(inputValue) = "" Then
                    CSngX = 0
                ElseIf inputValue Is Nothing Then
                    CSngX = 0
                ElseIf IsNumeric(inputValue) = True Then
                    CSngX = CSng(inputValue)
                Else
                    CSngX = 0
                End If
            Catch
                CSngX = 0
            End Try
        End Function

        Public ReadOnly Property Version() As String
            Get
                Dim sTemp As String
                sTemp = Trim(System.Reflection.Assembly.GetExecutingAssembly.FullName.Split(",")(1).Replace("Version=", ""))
                Return "v" & Mid(sTemp, 1, InStrRev(sTemp, ".") - 1) '& " - Build " & Mid(sTemp, InStrRev(sTemp, ".") + 1)
            End Get

        End Property

        Public Function AES_Decrypt(ByVal input As String, ByVal pass As String) As String
            Dim AES As New System.Security.Cryptography.RijndaelManaged
            Dim Hash_AES As New System.Security.Cryptography.MD5CryptoServiceProvider
            Dim decrypted As String = ""
            Try
                Dim hash(31) As Byte
                Dim temp As Byte() = Hash_AES.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(pass))
                Array.Copy(temp, 0, hash, 0, 16)
                Array.Copy(temp, 0, hash, 15, 16)
                AES.Key = hash
                AES.Mode = Security.Cryptography.CipherMode.ECB
                Dim DESDecrypter As System.Security.Cryptography.ICryptoTransform = AES.CreateDecryptor
                Dim Buffer As Byte() = Convert.FromBase64String(input)
                decrypted = System.Text.ASCIIEncoding.ASCII.GetString(DESDecrypter.TransformFinalBlock(Buffer, 0, Buffer.Length))
                Return decrypted
            Catch ex As Exception
                Return input
            End Try
        End Function

        Public Function BuildStatement(ByVal tableName As String, ByVal columns As Collection, ByVal values As Collection, Optional ByVal isInsert As Boolean = True, Optional ByVal whereStatement As String = "") As String
            Dim nCount As Integer
            Dim sColumns As String = ""
            Dim sValues As String = ""

            If isInsert = True Then
                For nCount = 1 To columns.Count
                    If sColumns <> "" Then
                        sColumns = sColumns & ","
                    End If
                    sColumns = sColumns & columns(nCount)
                Next

                For nCount = 1 To values.Count
                    If sValues <> "" Then
                        sValues = sValues & ","
                    End If
                    If values(nCount).ToString = "NULL" Then
                        sValues = sValues & "NULL"
                    ElseIf IsDBNull(values(nCount)) = True Then
                        sValues = sValues & "NULL"
                    Else
                        sValues = sValues & "'" & CDBValid(values(nCount)) & "'"
                    End If
                Next

                BuildStatement = "INSERT INTO " & tableName & " (" & sColumns & ") VALUES (" & sValues & ")"
            Else
                For nCount = 1 To columns.Count
                    If sColumns <> "" Then
                        sColumns = sColumns & ","
                    End If
                    sColumns = sColumns & columns(nCount) & " = " & "'" & CDBValid(values(nCount)) & "'"
                Next

                BuildStatement = "UPDATE " & tableName & " SET " & sColumns & " WHERE " & whereStatement
            End If

        End Function

    End Module
End NameSpace