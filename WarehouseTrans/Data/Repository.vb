﻿Imports System.Data.SqlClient
Imports WarehouseTrans.Extensions
Imports WarehouseTrans.Models

Namespace Data

    Public Class Repository
        Private _cnnString As String
        Public Sub New() 
            _cnnString = fsprdConnString
        End Sub
        Public Sub New(ByVal conn as string) 
            _cnnString = conn
        End Sub

        Public Function GetUser(winID as String) As BulkUser
            Dim ret As BulkUser = New BulkUser()
            Try
                Dim sqlst = "SELECT " & _
                        "UID, Name, FirstName, LastName, Plant, Enabled, " & _
                        "CanTransfer, CanBinMove, CanWeigh, CanHotList, CanMoveBulk, " & _
                        "CanMaterialAdjust, CanEditUsers, CanPrintLists, CanCreatePallet, " & _
                        "CanPalletMove, CanPlantTransfer, IsAdmin, ActiveDate, " & _
                        "ADName " & _
                        "FROM dbo.PBM_BULK_USERS WITH (NOLOCK) " & _
                        "WHERE ADName = @adname " & _
                        "AND Enabled = 1"
                Using sqlConnection = New SqlConnection(_cnnString)
                    Dim cmd As SqlCommand = New SqlCommand(sqlst, sqlConnection)
                    Dim parm = New SqlParameter("@adname", SqlDbType.VarChar, 100) With {.Value = winID}
                    cmd.Parameters.Add(parm)
                    If sqlConnection.State <> ConnectionState.Open Then sqlConnection.Open()
                    Dim rdr = cmd.ExecuteReader()
                    If rdr.HasRows Then
                        rdr.Read()
                        ret = New BulkUser(CType(rdr, IDataRecord))
                    End If

                    rdr.Close()
                    If sqlConnection.State = ConnectionState.Open Then sqlConnection.Close()
                End Using
            Catch ex As Exception
                'log.Info($"Exception caught: {ex.Message}")
                'log.Info(ex.StackTrace)
                Debug.Print($"Exception caught: {ex.Message}")
                Debug.Print(ex.StackTrace)
            End Try
            Return ret
        End Function
    End Class

End Namespace

