﻿Imports System.Data.SqlClient

Namespace Data

    Public Class Post
        Private _objConn As SqlConnection
        Private _gPlant as String
        Public Sub New() 
            _objConn = objConn
            _gPlant = "CL22"
        End Sub
        Public Sub New(ByVal conn as SQLConnection, ByVal plant As String) 
            _objConn = conn
            _gPlant = plant
        End Sub
        Public Sub UpdateLocation(ByVal material As String, 
                                  ByVal newBin As String, 
                                  ByVal functionName As String, 
                                  Optional ByVal SLOC As String = "410", 
                                  Optional ByVal existingBin As String = "", 
                                  Optional ByVal reason As String = "", 
                                  Optional ByVal reasonType As Integer = 1)

            Dim rsLocal As DataTable
            Dim nUID As Integer = 0

            Try
                rsLocal = SqlQuery("SELECT BIN FROM PBM_LOCATIONS LOC WITH (NOLOCK) WHERE MATERIAL_ID = '" & UCase(CDBValid(material)) & "' AND PLANT = '" & _gPlant & "' AND SLOC = '" & SLOC & "'", objConn)
                If rsLocal.Rows.Count = 0 Then
'                    SqlExecute("INSERT INTO PBM_LOCATIONS (MATERIAL_ID,PLANT,SLOC,BIN,ON_HAND,UNIT,IN_TRANSIT,TRANS_UNIT,BLOCKED,CREATE_DATE) VALUES ('" & UCase(CDBValid(material)) & "','" & gPlant & "','" & SLOC & "','" & UCase(newBin) & "',0,'EA',0,'EA',0,GetDate())", objConn)
                Else
                    SqlExecute("UPDATE PBM_LOCATIONS SET BIN = '" & Trim(newBin) & "' WHERE MATERIAL_ID = '" & UCase(CDBValid(material)) & "' AND PLANT = '" & _gPlant & "' AND SLOC = '" & SLOC & "'", objConn)
                End If
            Catch
            End Try

            Try
                SqlExecute("UPDATE PBM_BULK_LOCATIONS SET Bin = '" & Trim(newBin) & "' WHERE SAPLocation = 1 AND Material = '" & UCase(CDBValid(material)) & "' AND PLANT = '" & _gPlant & "' AND SLOC = '" & SLOC & "'", objConn)
            Catch
                'MsgBox()
            End Try

            If Trim(reason) <> "" Then
                rsLocal = SqlQuery("SELECT UID FROM PBM_BULK_REASON_CODE WHERE UPPER(ReasonText) = '" & UCase(CDBValid(reason)) & "' AND ReasonType = " & reasonType, objConn)
                If rsLocal.Rows.Count = 0 Then
                    SqlExecute("INSERT INTO PBM_BULK_REASON_CODE (ReasonText,ReasonType,SortOrder) VALUES ('" & CDBValid(reason) & "'," & reasonType & ")", objConn)
                    rsLocal = SqlQuery("SELECT UID FROM PBM_BULK_REASON_CODE WHERE UPPER(ReasonText) = '" & UCase(CDBValid(reason)) & "' AND ReasonType = " & reasonType, objConn)
                    nUID = CLngX(rsLocal.Rows(0).Item(0).ToString)
                Else
                    nUID = CLngX(rsLocal.Rows(0).Item(0).ToString)
                End If
            Else
                nUID = 0
            End If

'            SqlExecute("INSERT INTO PBM_BULK_LOC_MOVE (Material,Plant,StartingBin,EndingBin,UserUID,MoveDate,MoveType,ReasonCode,ComputerName,Completed) VALUES ('" & UCase(CDBValid(material)) & "','" & _gPlant & "','" & UCase(Trim(existingBin)) & "','" & UCase(Trim(newBin)) & "','" & gUser & "',GetDate(),'" & functionName & "'," & IIf(nUID = 0, "NULL", nUID) & ",'" & gUnit & "',0)", objConn)
        End Sub

    End Class
End NameSpace