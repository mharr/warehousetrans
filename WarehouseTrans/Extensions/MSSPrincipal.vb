﻿Imports System.DirectoryServices.AccountManagement
Imports System.Security.Principal
Imports System.Web.UI.WebControls.Expressions
Imports Microsoft.Ajax.Utilities
Imports WarehouseTrans.Data

Namespace Extensions

    Public Class MSSPrincipal
        Implements IPrincipal

        Private ReadOnly _repository As Repository

        Public ReadOnly Property Name As String
        Public ReadOnly Property ADName as String 
        Public ReadOnly Property Plant as String 
        Public ReadOnly Property IsAdmin As Boolean
        Public ReadOnly Property Roles As List(Of WarehouseTransRoles) = New List(Of WarehouseTransRoles)

        Public Function IsInRole(role As String) As Boolean Implements IPrincipal.IsInRole
            'Admin always authorized
            If IsAdmin Then Return True
            'If no roles, and not admin, fail
            If Roles.Count = 0 Then Return False
            'If asking if in any role, allow
            If role = "*" Or role.ToLower() = "any" Then Return True
            if Roles.Any(Function(r) [Enum].GetName(r.[GetType](), r) = role) Then
                Return True
            End If

            Dim winprin = New WindowsPrincipal(CType(Identity, WindowsIdentity))
            If winprin.IsInRole(role) Then
                Return True
            End If

            Return False
        End Function

        Public Function IsInRole(role as WarehouseTransRoles)
            If IsAdmin Then Return True
            Return Roles.Any(Function(r) [Enum].GetName(r.[GetType](), r) = role) 
        End Function

        Public ReadOnly Property Identity As IIdentity Implements IPrincipal.Identity
    
        Public Sub New(username As String)
            _repository = New Repository()
            Me.Identity = New GenericIdentity(username)
            Dim winuser = GetWindowsPrincipal(username)
            If winuser IsNot Nothing Then
                Init(winuser)
            End If
        End Sub

        Public Sub New(ByVal user As WindowsPrincipal)
            _repository = New Repository()
            Me.Identity = user.Identity
            Init(user)
        End Sub

        Public Sub New(ByVal user As WindowsPrincipal, repository As Repository)
            _repository = repository
            Me.Identity = user.Identity
            Init(user)
        End Sub

        Private Sub Init(ByVal winuser As WindowsPrincipal)
            Dim bulkUser = _repository.GetUser(winuser.Identity.Name)
            _Name = bulkUser.LastName & If(bulkUser.FirstName Is Nothing, "", ", " & bulkUser.FirstName )
            _ADName = bulkUser.ADName
            _Plant = bulkUser.Plant
            _IsAdmin = bulkUser.IsAdmin
            If bulkUser.CanTransfer Then Roles.Add(WarehouseTransRoles.CanTransfer)
            If bulkUser.CanBinMove Then Roles.Add(WarehouseTransRoles.CanBinMove)
            If bulkUser.CanWeigh  Then Roles.Add(WarehouseTransRoles.CanWeigh )
            If bulkUser.CanHotlist  Then Roles.Add(WarehouseTransRoles.CanHotlist )
            If bulkUser.CanMoveBulk  Then Roles.Add(WarehouseTransRoles.CanMoveBulk )
            If bulkUser.CanMaterialAdjust  Then Roles.Add(WarehouseTransRoles.CanMaterialAdjust )
            If bulkUser.CanEditUsers  Then Roles.Add(WarehouseTransRoles.CanEditUsers )
            If bulkUser.CanPrintLists  Then Roles.Add(WarehouseTransRoles.CanPrintLists )
            If bulkUser.CanCreatePallet  Then Roles.Add(WarehouseTransRoles.CanCreatePallet )
            If bulkUser.CanPalletMove  Then Roles.Add(WarehouseTransRoles.CanPalletMove )
            If bulkUser.CanPlantTransfer  Then Roles.Add(WarehouseTransRoles.CanPlantTransfer )
        End Sub

        Private Shared Function GetWindowsPrincipal(ByVal username As String) As WindowsPrincipal
            Using user = If(UserPrincipal.FindByIdentity(UserPrincipal.Current.Context, IdentityType.SamAccountName, username), UserPrincipal.FindByIdentity(UserPrincipal.Current.Context, IdentityType.UserPrincipalName, username))
                Return If(user Is Nothing, Nothing, New WindowsPrincipal(New WindowsIdentity(user.UserPrincipalName)))
            End Using
        End Function

    End Class

    Public Enum WarehouseTransRoles
        CanTransfer
        CanBinMove
        CanWeigh 
        CanHotlist 
        CanMoveBulk 
        CanMaterialAdjust 
        CanEditUsers 
        CanPrintLists 
        CanCreatePallet 
        CanPalletMove 
        CanPlantTransfer 
    End Enum
End NameSpace