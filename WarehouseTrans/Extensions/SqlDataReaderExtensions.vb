﻿
Imports System.Data
Imports System.Data.SqlClient

Namespace Extensions

    Module IDataRecordExtensions

        Function SafeGetString(ByVal reader As IDataRecord, ByVal colIndex As Integer) As String
            If reader.IsDBNull(colIndex) Then
                Return String.Empty
            Else
                Return reader.GetString(colIndex)
            End If
        End Function

        Function SafeGetString(ByVal reader As IDataRecord, ByVal colName As String) As String
            Return SafeGetString(reader, reader.GetOrdinal(colName))
        End Function

        Function SafeGetNullInt(ByVal reader As IDataRecord, ByVal colIndex As Integer) As Integer
            If reader.IsDBNull(colIndex) Then
                Return -1
            Else
                Return reader.GetInt32(colIndex)
            End If
        End Function

        Function SafeGetNullInt(ByVal reader As IDataRecord, ByVal colName As String) As Integer
            Return SafeGetNullInt(reader, reader.GetOrdinal(colName))
        End Function

        Function SafeGetNullBigInt(ByVal reader As IDataRecord, ByVal colIndex As Integer) As Long
            If reader.IsDBNull(colIndex) Then
                Return -1
            Else
                Return reader.GetInt64(colIndex)
            End If
        End Function

        Function SafeGetNullBigInt(ByVal reader As IDataRecord, ByVal colName As String) As Long
            Return SafeGetNullBigInt(reader, reader.GetOrdinal(colName))
        End Function

        Function SafeGetDateTime(ByVal reader As IDataRecord, ByVal colIndex As Integer) As DateTime
            If reader.IsDBNull(colIndex) Then
                Return DateTime.MinValue
            Else
                Return reader.GetDateTime(colIndex)
            End If
        End Function

        Function SafeGetDateTime(ByVal reader As IDataRecord, ByVal colName As String) As DateTime
            Return SafeGetDateTime(reader, reader.GetOrdinal(colName))
        End Function

        Function SafeGetBoolean(ByVal reader As IDataRecord, ByVal colIndex As Integer) As Boolean
            If reader.IsDBNull(colIndex) Then
                Return False
            Else
                Select Case reader.GetFieldType(colIndex)
                    Case GetType(Boolean)
                        Return reader.GetBoolean(colIndex)
                    Case GetType(Integer), GetType(Long), GetType(Decimal), gettype(Single), GetType(Double)
                        If reader.GetValue(colIndex) = 0 Then
                            Return False
                        Else
                            Return True
                        End If
                    Case GetType(String)
                        Select Case reader.GetString(colIndex).ToUpper().Chars(0)
                            Case "Y", "T"
                                Return True
                            Case Else
                                Return False
                        End Select
                    Case Else
                        Return False
                End Select
            End If
        End Function

        Function SafeGetBoolean(ByVal reader As IDataRecord, ByVal colName As String) As Boolean
            Return SafeGetBoolean(reader, reader.GetOrdinal(colName))
        End Function

        Function SafeGetDecimal(ByVal reader As IDataRecord, ByVal colIndex As Integer, ByVal Optional nullVal As Integer = -1) As Decimal
            If reader.IsDBNull(colIndex) Then
                Return nullVal
            Else
                Return reader.GetDecimal(colIndex)
            End If
        End Function

        Function SafeGetDecimal(ByVal reader As IDataRecord, ByVal colName As String, ByVal Optional nullVal As Integer = -1) As Decimal
            Return SafeGetDecimal(reader, reader.GetOrdinal(colName), nullVal)
        End Function

        Function SafeGetInt32(ByVal reader As IDataRecord, ByVal colIndex As Integer, ByVal Optional nullVal As Integer = -1) As Int32
            If reader.IsDBNull(colIndex) Then
                Return nullVal
            Else
                Return reader.GetInt32(colIndex)
            End If
        End Function

        Function SafeGetInt32(ByVal reader As IDataRecord, ByVal colName As String, ByVal Optional nullVal As Integer = -1) As Int32
            Return SafeGetInt32(reader, reader.GetOrdinal(colName), nullVal)
        End Function
    End Module
End NameSpace