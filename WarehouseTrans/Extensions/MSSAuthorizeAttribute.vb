﻿
Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Linq
Imports System.Web
Imports System.Web.Mvc
Imports System.Web.Routing
Imports System.Web.UI.WebControls

Namespace Extensions

    <AttributeUsage(AttributeTargets.Method Or AttributeTargets.[Class], Inherited:=True, AllowMultiple:=True)>
    Public Class MSSAuthorizeAttribute
        Inherits AuthorizeAttribute

        Protected Overridable ReadOnly Property CurrentUser As MSSPrincipal
            Get
                Return TryCast(HttpContext.Current.User, MSSPrincipal)
            End Get
        End Property

        Public Sub New(ParamArray roles As Object())
            If roles.Any(Function(r) r.[GetType]().BaseType <> GetType([Enum])) Then Throw New ArgumentException("roles")
            Me.Roles = String.Join(",", roles.[Select](Function(r) [Enum].GetName(r.[GetType](), r)))
        End Sub

        Protected Overrides Function AuthorizeCore(ByVal httpContext As HttpContextBase) As Boolean
            If httpContext Is Nothing Then Throw New ArgumentNullException("httpContext")
            If Not httpContext.User.Identity.IsAuthenticated Then Return False
            Return Check_IsInRole(httpContext)
        End Function

        Private Function Check_IsInRole(ByVal httpContext As HttpContextBase) As Boolean
            If httpContext.User.IsInRole("IsAdmin") Then
                Return True
            End If

            If Roles.Length = 0 Then Return True
            Return Roles.Split(","c).Any(Function(r) httpContext.User.IsInRole(r) = True)
        End Function

        Protected Overrides Sub HandleUnauthorizedRequest(ByVal filterContext As AuthorizationContext)
            If filterContext.RequestContext.HttpContext.User.Identity.IsAuthenticated Then
                filterContext.Result = New ViewResult With {.MasterName = "_Layout", .ViewName = "Unauthorized"}
            Else
                MyBase.HandleUnauthorizedRequest(filterContext)
            End If
        End Sub

    End Class
End NameSpace