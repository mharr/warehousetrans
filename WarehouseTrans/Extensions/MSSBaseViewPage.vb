﻿
Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Security.Principal
Imports System.Web
Imports System.Web.Mvc

Namespace Extensions

    Public MustInherit Class BaseViewPage
        Inherits WebViewPage

        Public Overridable ReadOnly Shadows Property User As MSSPrincipal
            Get
                Return TryCast(MyBase.User, MSSPrincipal)
            End Get
        End Property
    End Class

    Public MustInherit Class BaseViewPage(Of TModel)
        Inherits WebViewPage(Of TModel)

        Public Overridable ReadOnly Shadows Property User As MSSPrincipal
            Get
                Return TryCast(MyBase.User, MSSPrincipal)
            End Get
        End Property
    End Class
End Namespace
