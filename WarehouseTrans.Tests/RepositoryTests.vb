﻿Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Web.Mvc
Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports WarehouseTrans
Imports WarehouseTrans.Data

<TestClass()> Public Class RepositoryTests

    <TestMethod()> Public Sub GetUser()
        ' Arrange
        Dim obj As New Repository(WarehouseTrans.Tests.Data.fsprdConnString)
        Dim usr As String = "appliedglobal\mharr"
        ' Act
        Dim result = obj.GetUser(usr)

        ' Assert
        Assert.IsNotNull(result)
        Assert.AreEqual(usr, result.ADName)
    End Sub

    <TestMethod()> Public Sub GetUser_Fails()
        ' Arrange
        Dim obj As New Repository(WarehouseTrans.Tests.Data.fsprdConnString)
        Dim usr As String = "NOT_THERE"
        ' Act
        Dim result = obj.GetUser(usr)

        ' Assert
        Assert.IsNotNull(result)
        Assert.IsNull(result.ADName)
    End Sub
End Class
